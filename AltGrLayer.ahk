; ========================== HotKeys ===============================================
; ==================================================================================

RAlt & F1::KeyHistory
RAlt & F2::Reload

; ========================== Alternative Alt Gr Remaps =============================
; ==================================================================================

; ! " £ $ %  ^ & * _ 
; + [ { ( <  > ) } ] = -

;bring usefull shift number characters to the top keyboard row 
RAlt & SC010:: Send {ShiftDown}{1}{ShiftUp}
RAlt & SC011:: Send {ShiftDown}{2}{ShiftUp}
RAlt & SC012:: Send {ShiftDown}{3}{ShiftUp}
RAlt & SC013:: Send {ShiftDown}{4}{ShiftUp}
RAlt & SC014:: Send {ShiftDown}{5}{ShiftUp}
RAlt & SC015:: Send {ShiftDown}{6}{ShiftUp}
RAlt & SC016:: Send {ShiftDown}{7}{ShiftUp}
RAlt & SC017:: Send {ShiftDown}{8}{ShiftUp}
RAlt & SC018:: Send {ShiftDown}{-}{ShiftUp}
RAlt & SC019:: Send {ShiftDown}{-}{ShiftUp}

;brackets to home keys (a,s,d,f,g  h,j,k,l,;,'
RAlt & SC01E::  Send {+}
RAlt & SC01F::  Send {[}
RAlt & SC020::  Send {{}
RAlt & SC021::  Send {(}
RAlt & SC022::  Send {<}

RAlt & SC023::  Send {>}
RAlt & SC024::  Send {)}
RAlt & SC025::  Send {}}
RAlt & SC026::  Send {]}
RAlt & SC027::  Send {=}
RAlt & SC028::  Send {-}
