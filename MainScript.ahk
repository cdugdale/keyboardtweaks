; ========================== Main Script ===========================================
; ==================================================================================
; ========================== Includes ==========================================

;This script is provided primarily to allow usage of the other scripts and show the import method.
;You can of course add your own scripts below, or import them should you wish. 

#Include, CapsLayer.ahk
#Include, AltGrLayer.ahk
#Include, ApplicationNavigation.ahk