KeyboardTweaks
==============

A collection of keyboard tweaks using autohotkey (http://www.autohotkey.com) scripts I have made and that I have found useful for programming and also for every day use for navigation and symbol access without leaving the home keys. Below are further details of the caps layer, the Alt Gr layer, and my PKL setup.

One of the main aims of the setup is to keep the shortcut keys the same regardless of keyboard layout. I use Colemak (using Portable Keyboard Layout http://pkl.sourceforge.net) on my laptop which is where i do most of my article writing for websites, and I use qwerty for my day job as a developer. This is because I didn't want to relearn all of the shortcuts for Visual Studio as well as a host of other tools that I use every day. I also use Colemak at work for drafting longer docs.

The solution I have found is a combination of a modified Portable Keyboard Coiemak layout which maps the scan codes for the keys to the original keys and some AHK remaps to handle the additional layers.

Originally I started this as a PKL script but decided to separate it into some AHK scripts for general use, and also thought it would be easier to allow others to use them when they were in an AHK file as opposed to a PKL layout file.


##Caps layer
The caps layer allous the use of the capslock key as a modifier and the ISO-CAPS-Extend.png shows the layout that is in the files at the moment. 

The caps layer includes movement keys, mouse keys, mouse scroll movement, next/ prev tab keys, copy, paste etc as well as a few others.

##Alt Gr layer
The alt Gr layer is for moving the most used programming symbols onto or close to the homekeys, and also moving the symbols from the number row down one to the top row. The layout is as follows:

The top layer has the % on T and the ^ on Y, the home row has the < on G and the > on H, the rest should be quite easy to follow from there.

! " - / %    ^ & * _ _

\+ [ { ( <    > ) } ] =

##Application Navigation

This is for opening files, or switching between open and inactive windows. 

##Base file

This allows the repo to be cloned, and then the base file run as a new ahk script which will include the other ones. it is just for convenience sake, but you can of course add anything you would like to the script and it will function like any other script.

##Disclaimer

Please be aware that this might not be actively updated, or that it might change without notice. It is mostly just somewhere for me to keep the files and to have them accessible from anywhere, but I've spent tens of hours sorting this all out so I thought it might be useful for someone. :)

Enjoy! 

Chris Dugdale 

christopher.dugdale@gmail.com
chrisdugdale.net


##Credits
Danik for some of the caps keys ideas and initial code. More info at http://danikgames.com/blog/?p=714 and https://gist.github.com/Danik/5808330

The image for the keys is a modified version of one from WikiPedia, the licence allows reuse as per here: https://commons.wikimedia.org/wiki/File:Qwerty.svg 
