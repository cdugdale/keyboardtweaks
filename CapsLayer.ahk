; ========================== Capslock remapping to modifier ========================
; ==================================================================================
; Original caps lock script by Danik: More info at http://danikgames.com/blog/?p=714
; https://gist.github.com/Danik/5808330
;  
; Functionality:
; - Deactivates capslock for normal (accidental) use.
; - Hold Capslock and drag anywhere in a window to move it (not just the title bar).
; - Access the following functions when pressing Capslock: 
;     Cursor keys           - J, K, L, I
;     Home, PgDn, PgUp, End - U, O, Y, H
;     Backspace and Del     - ;, '
;     Mouse Wheel U,D,L,R   - W, S, A, D
;   MButtons 1, 2, 3      - M, ,, .
;     Next, previous tab    - Tab, Q
;  
; To use capslock as you normally would, you can press WinKey + Capslock
 
#Persistent
SetCapsLockState, Alwaysoff
CapsLock:: return

;Scan code 03a is what the Samsung S9 has for the Capslock key. Using the capslock key itself shows the 'capslock on/off' popup
;F Keys onto key row (1,2,3,4,5,6,7,8,9,0,-,=)
SC03A & SC002::SendInput {F1}
SC03A & SC003::SendInput {F2}
SC03A & SC004::SendInput {F3}
SC03A & SC005::SendInput {F4}
SC03A & SC006::SendInput {F5}
SC03A & SC007::SendInput {F6}
SC03A & SC008::SendInput {F7}
SC03A & SC009::SendInput {F8}
SC03A & SC00A::SendInput {F9}
SC03A & SC00B::SendInput {F10}
SC03A & SC00C::SendInput {F11}
SC03A & SC00D::SendInput {F12}

; Capslock + ijkl (Up, down, left, right cursor keys)
SC03A & SC017::Send {Blind}{Up Down}
SC03A & SC017 up::Send {Blind}{Up Up}
SC03A & SC024::Send {Blind}{Left Down}
SC03A & SC024 up::Send {Blind}{Left Up}
SC03A & SC025::Send {Blind}{Down Down}
SC03A & SC025 up::Send {Blind}{Down Up}
SC03A & SC026::Send {Blind}{Right Down}
SC03A & SC026 up::Send {Blind}{Right Up}


; SC03A + uo (home, end)
SC03A & SC016::SendInput {Blind}{Home Down}
SC03A & SC016 up::SendInput {Blind}{Home Up}
SC03A & SC018::SendInput {Blind}{End Down}
SC03A & SC018 up::SendInput {Blind}{End Up}

; SC03A + hy (pgup, pgdown)
SC03A & SC023::SendInput {Blind}{PgDn Down}
SC03A & SC023 up::SendInput {Blind}{PgDn Up}
SC03A & SC015::SendInput {Blind}{PgUp Down}
SC03A & SC015 up::SendInput {Blind}{PgUp Up} 

; SC03A + wsad (Navigate with mouse wheel keys)
SC03A & SC011::SendInput {WheelUp}
SC03A & SC01F::SendInput {WheelDown}
SC03A & SC01E:: 
  ; Scroll to the left
  MouseGetPos,,,id, fcontrol,1
  Loop 8 ; <-- Increase for faster scrolling
    SendMessage, 0x114, 0, 0, %fcontrol%, ahk_id %id% ; 0x114 is WM_HSCROLL and the 0 after it is SB_LINERIGHT.
return

SC03A & SC020:: 
  ;Scroll to the right
  MouseGetPos,,,id, fcontrol,1
  Loop 8 ; <-- Increase for faster scrolling
    SendMessage, 0x114, 1, 0, %fcontrol%, ahk_id %id% ;  0x114 is WM_HSCROLL and the 1 after it is SB_LINELEFT.
return

; SC03A + ; p BS(insert, backspace, del)`
SC03A & SC027::SendInput {Blind}{BS Down}
SC03A & SC00E::SendInput {Blind}{Del Down}
 
; Make SC03A & Enter equivalent to Control+Enter
SC03A & Enter::SendInput {Ctrl down}{Enter}{Ctrl up}
 
; Make SC03A & Alt Equivalent to Control+Alt
!SC03A::SendInput {Ctrl down}{Alt Down}
!SC03A up::SendInput {Ctrl up}{Alt up}
 
; SC03A + TAB/q (prev/next tab)
SC03A & SC029::SendInput {Ctrl Down}{Shift Down}{Tab Down}
SC03A & SC029 up::SendInput {Ctrl Up}{Shift Up}{Tab Up}
SC03A & SC00F::SendInput {Ctrl Down}{Tab Down}
SC03A & SC00F up::SendInput {Ctrl Up}{Tab Up}

; SC03A + m,. (Mouse buttons on keyboard)
SC03A & SC032::SendInput {LButton}
SC03A & SC033::SendInput {MButton}
SC03A & SC034::SendInput {RButton}

; SC03A + z\ (undo, redo)
;caps shift z will redo, so will caps and \
SC03A & SC02C::
  GetKeyState, sh, Shift
  if sh = D
    Send {Ctrl Down}{SC015 Down}
  else
    Send {Ctrl Down}{SC02C Down}
return

SC03A & SC02C Up::
    Send {Ctrl Up}{SC015 Up}
    Send {Ctrl Up}{SC02C Up}
return
SC03A & SC056::Send {Ctrl Down}{SC015 Down}
SC03A & SC056 Up::Send {Ctrl Up}{SC015 Up}

; F R E Q  ctrl shift and Esc 
SC03A & SC021::Send {Ctrl Down}
SC03A & SC021 Up::Send {Ctrl Up}
SC03A & SC010::SendInput {Escape}{Escape}
SC03A & SC010 up::SendInput {Escape}{Escape}
SC03A & SC012::Send {Esc Down}
SC03A & SC012 Up::Send {Esc Up}
SC03A & SC013::Send {Shift Down}
SC03A & SC013 Up::Send {Shift Up}

;Browser Navigate
;SC03A & SC012::Browser_Back ;e
;SC03A & SC013::Browser_Forward ;r

SC03A & SC035::SendInput {Blind}{Enter}
SC03A & SC028::SendInput {Blind}{Del Down}

; SC03A + xcv (cut, copy, paste)
SC03A & SC02D:: SendInput {Ctrl Down}{SC02D Down}
SC03A & SC02D up:: SendInput {Ctrl Up}{SC02D Up}
SC03A & SC02E:: SendInput {Ctrl Down}{SC02E Down}
SC03A & SC02E up:: SendInput {Ctrl Up}{SC02E Up}
SC03A & SC02F:: SendInput {Ctrl Down}{SC02F Down}
SC03A & SC02F up:: SendInput {Ctrl Up}{SC02F Up}

;Winkey and Caps (Capslock)
#SC03a :: CAPSLOCK

; Currently unused keys are mapped to do nothing to prevent accidental text entry
;SC03A & SC012::return ;e
;SC03A & SC013::return ;r
SC03A & SC014::return ;t
SC03A & SC019::return ;p
SC03A & SC01A::return ;[
SC03A & SC01B::return ;]
;SC03A & SC021::return ;f
;SC03A & SC022::return ;g
;SC03A & SC028::return ;'
SC03A & SC02B::return ;#
SC03A & SC030::return ;b
SC03A & SC031::return ;n
;SC03A & SC035::return ;/




; ================================= Media Keys =====================================
; ==================================================================================
;useful in case keyboard has no media keys. saves restoring a media windows just to change track. 

;previous song
SC03A & F1::
Send {Media_Prev}
return

;play/pause
SC03A & F2::
Send {Media_Play_Pause}
return

;stop
SC03A & F3::
Send {Media_Stop}
return

;next song
SC03A & F4::
Send {Media_Next}
return


; ========================== Drag windows anywhere =================================
; ==================================================================================
; This mouse move script from: http://www.autohotkey.com/docs/scripts/EasyWindowDrag.htm
 
~MButton & LButton::
SC03a & LButton::
CoordMode, Mouse  ; Switch to screen/absolute coordinates.
MouseGetPos, EWD_MouseStartX, EWD_MouseStartY, EWD_MouseWin
WinGetPos, EWD_OriginalPosX, EWD_OriginalPosY,,, ahk_id %EWD_MouseWin%
WinGet, EWD_WinState, MinMax, ahk_id %EWD_MouseWin% 
if EWD_WinState = 0  ; Only if the window isn't maximized
    SetTimer, EWD_WatchMouse, 10 ; Track the mouse as the user drags it.
return

EWD_WatchMouse:
GetKeyState, EWD_LButtonState, LButton, P
if EWD_LButtonState = U  ; Button has been released, so drag is complete.
{
    SetTimer, EWD_WatchMouse, off
    return
}
GetKeyState, EWD_EscapeState, Escape, P
if EWD_EscapeState = D  ; Escape has been pressed, so drag is cancelled.
{
    SetTimer, EWD_WatchMouse, off
    WinMove, ahk_id %EWD_MouseWin%,, %EWD_OriginalPosX%, %EWD_OriginalPosY%
    return
}
; Otherwise, reposition the window to match the change in mouse coordinates
; caused by the user having dragged the mouse:
CoordMode, Mouse
MouseGetPos, EWD_MouseX, EWD_MouseY
WinGetPos, EWD_WinX, EWD_WinY,,, ahk_id %EWD_MouseWin%
SetWinDelay, -1   ; Makes the below move faster/smoother.
WinMove, ahk_id %EWD_MouseWin%,, EWD_WinX + EWD_MouseX - EWD_MouseStartX, EWD_WinY + EWD_MouseY - EWD_MouseStartY
EWD_MouseStartX := EWD_MouseX  ; Update for the next timer-call to this subroutine.
EWD_MouseStartY := EWD_MouseY
return
;
;CAPSLOCK & LButton::
;CoordMode, Mouse  ; Switch to screen/absolute coordinates.
;MouseGetPos, EWD_MouseStartX, EWD_MouseStartY, EWD_MouseWin
;WinGetPos, EWD_OriginalPosX, EWD_OriginalPosY,,, ahk_id %EWD_MouseWin%
;WinGet, EWD_WinState, MinMax, ahk_id %EWD_MouseWin% 
;if EWD_WinState = 0  ; Only if the window isn't maximized a
;    SetTimer, EWD_WatchMouse, 10 ; Track the mouse as the user drags it.
;return
; 
;EWD_WatchMouse:
;GetKeyState, EWD_LButtonState, LButton, P
;if EWD_LButtonState = U  ; Button has been released, so drag is complete.
;{
;    SetTimer, EWD_WatchMouse, off
;    return
;}
;GetKeyState, EWD_EscapeState, Escape, P
;if EWD_EscapeState = D  ; Escape has been pressed, so drag is cancelled.
;{
;    SetTimer, EWD_WatchMouse, off
;    WinMove, ahk_id %EWD_MouseWin%,, %EWD_OriginalPosX%, %EWD_OriginalPosY%
;    return
;}
;; Otherwise, reposition the window to match the change in mouse coordinates
;; caused by the user having dragged the mouse:
;CoordMode, Mouse
;MouseGetPos, EWD_MouseX, EWD_MouseY
;WinGetPos, EWD_WinX, EWD_WinY,,, ahk_id %EWD_MouseWin%
;SetWinDelay, -1   ; Makes the below move faster/smoother.
;WinMove, ahk_id %EWD_MouseWin%,, EWD_WinX + EWD_MouseX - EWD_MouseStartX, EWD_WinY + EWD_MouseY - EWD_MouseStartY
;EWD_MouseStartX := EWD_MouseX  ; Update for the next timer-call to this subroutine.
;EWD_MouseStartY := EWD_MouseY
;return
;