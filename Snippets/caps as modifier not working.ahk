
;Use Capslock as a modifier and not as capslock anymore
$*Capslock::
    Gui, 99:+ToolWindow
    Gui, 99:Show, x-1 w1 +NoActivate, Capslock Is Down
    keywait, Capslock
    Gui, 99:Destroy
    return

;Write functions for keys while capslock is beeing hold here
#IfWinExist, Capslock Is Down
    y::Home
    u::Send PgDown
    i::PgUp
    o::End
    j::Down
    k::Up
    l::Right
    h::Left
    d::Delete
#IfWinExist

;Use right and left shifts to toggle capslock
RShift & LShift::
    SetCapsLockState, % (State:=!State) ? "On" : "Off"
    return

LShift & RShift::
    SetCapsLockState, % (State:=!State) ? "On" : "Off"
    return
