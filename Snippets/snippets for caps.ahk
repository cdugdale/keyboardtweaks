Sc03a & j::Send {Blind}{Left DownTemp}
SC03A & j up::Send {Blind}{Left Up}

SC03A & k::Send {Blind}{Down DownTemp}
SC03A & k up::Send {Blind}{Down Up}

SC03A & i::Send {Blind}{Up DownTemp}
SC03A & i up::Send {Blind}{Up Up}

SC03A & l::Send {Blind}{Right DownTemp}
SC03A & l up::Send {Blind}{Right Up}


; SC03A + uohy (pgdown, pgup, home, end)
 
SC03A & u::SendInput {Blind}{Home Down}
SC03A & u up::SendInput {Blind}{Home Up}
 
SC03A & o::SendInput {Blind}{End Down}
SC03A & o up::SendInput {Blind}{End Up}
 
SC03A & y::SendInput {Blind}{PgUp Down}
SC03A & y up::SendInput {Blind}{PgUp Up}
 
SC03A & h::SendInput {Blind}{PgDn Down}
SC03A & h up::SendInput {Blind}{PgDn Up}
 
 
; SC03A + asdf (select all, cut-copy-paste)
 
SC03A & a::SendInput {Ctrl Down}{a Down}
SC03A & a up::SendInput {Ctrl Up}{a Up}
 
SC03A & s::SendInput {Ctrl Down}{x Down}
SC03A & s up::SendInput {Ctrl Up}{x Up}
 
SC03A & d::SendInput {Ctrl Down}{c Down}
SC03A & d up::SendInput {Ctrl Up}{c Up}
 
SC03A & f::SendInput {Ctrl Down}{v Down}
SC03A & f up::SendInput {Ctrl Up}{v Up}
 
 
; SC03A + wer (close tab or window, press esc)
 
SC03A & w::SendInput {Ctrl down}{F4}{Ctrl up}
SC03A & e::SendInput {Alt down}{F4}{Alt up}
SC03A & r::SendInput {Blind}{Esc Down}

 
; SC03A + nm (insert, backspace, del)
 
SC03A & b::SendInput {Blind}{Insert Down}
SC03A & m::SendInput {Blind}{Del Down}
SC03A & n::SendInput {Blind}{BS Down}
SC03A & BS::SendInput {Blind}{BS Down}

 
; Make SC03A & Enter equivalent to Control+Enter
SC03A & Enter::SendInput {Ctrl down}{Enter}{Ctrl up}
 
 
; Make SC03A & Alt Equivalent to Control+Alt
!SC03A::SendInput {Ctrl down}{Alt Down}
!SC03A up::SendInput {Ctrl up}{Alt up}
 
 
; SC03A + TAB/q (prev/next tab)
 
SC03A & q::SendInput {Ctrl Down}{Tab Down}
SC03A & q up::SendInput {Ctrl Up}{Tab Up}
SC03A & Tab::SendInput {Ctrl Down}{Shift Down}{Tab Down}
SC03A & Tab up::SendInput {Ctrl Up}{Shift Up}{Tab Up}
 
; SC03A + ,/. (undo/redo)
 
SC03A & ,::SendInput {Ctrl Down}{z Down}
SC03A & , up::SendInput {Ctrl Up}{z Up}
SC03A & .::SendInput {Ctrl Down}{y Down}
SC03A & . up::SendInput {Ctrl Up}{y Up}
 
 
; Make SC03A+Space -> Enter
SC03A & Space::SendInput {Enter Down}

