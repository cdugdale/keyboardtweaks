    @ € [ ] #    ! < > = &
    \ ° { } *    ? ( ) +
      $ | ~ `    ^ % " '

As you'll notice, all pairs of brackets are conveviently located beneath index and middle finger on home- and top-row, and all other symbols well accessible. 

The 4th layer (Mod4) features all navigational keys on the left and numpad right:
    ⇞ ⇤ ↑ ⇥ ⇟    ß 7 8 9 ä     ⇤,⇥ : backspace & delete
    ⇤ ← ↓ → ⇥    . 4 5 6 ö
    e ⇥ i ⏎      0 1 2 3 ü 