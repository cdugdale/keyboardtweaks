; ========================== Application Navigation ================================
; ==================================================================================
; ========================== Method Calls ==========================================
;
;Just some application keys that allow navigation to other apps that are open.
;This needs to be modified to open the window if no window is found. 
;This will require hardcoded values for the install locoations of the exe files or programs. 

+!v::ToggleWinMinimize("Visual Studio")
;+!s::ToggleWinMinimize("Sublime Text")
+!q::ToggleWinMinimize("SQL Server Management Studio")
+!i::ToggleWinMinimize("Explorer")
;+!e::ToggleWinMinimize("ahk_class CabinetWClass")
+!c::ToggleWinMinimize("Chrome")

!+e::
IfWinExist, ahk_class CabinetWClass
	IfWinNotActive
	{
		WinActivate
		WinWaitActive, ahk_class CabinetWClass
		Send !d
	}
	;else
	;{
		;WinMinimize
	;}
else
{
	run explorer.exe computer
	WinWaitActive ahk_class CabinetWClass
	Send !d
}
return


; ==================================================================================
; ========================== Methods ===============================================


ToggleWinMinimize(TheWindowTitle)
{
	SetTitleMatchMode,2
	DetectHiddenWindows, Off
	IfWinActive, %TheWindowTitle%
	{
		WinMinimize, %TheWindowTitle%
	}
	Else
	{
		IfWinExist, %TheWindowTitle%
		{
			WinGet, winid, ID, %TheWindowTitle%
			DllCall("SwitchToThisWindow", "UInt", winid, "UInt", 1)
		}
	}
	Return
}



;MOUSE MOVE NOT WORKING

;SC02A & SC03A & SC017:: ;UP
;SC02A & SC03A & SC024:: ;Left
;SC02A & SC03A & SC025:: ;Right::
;SC02A & SC03A & SC026:: ;Down::
;    R := 0
;    Loop{
;	 X := Y := 0
;	 MouseMove, X := GetKeyState("SC03A & RALT & SC024","P") ? -++R : GetKeyState("SC03A & RALT & SC025","P") ? ++R : X
;		  , Y := GetKeyState("up","P") ? -++R : GetKeyState("down","P") ? ++R : Y,,R
;	}until !X && !Y
;return

;SC03A & SC017::Send {Blind}{Up Down}
;SC03A & SC017 up::Send {Blind}{Up Up}
;SC03A & SC024::Send {Blind}{Left Down}
;SC03A & SC024 up::Send {Blind}{Left Up}
;SC03A & SC025::Send {Blind}{Down Down}
;SC03A & SC025 up::Send {Blind}{Down Up}
;SC03A & SC026::Send {Blind}{Right Down}
;SC03A & SC026 up::Send {Blind}{Right Up}


;Up::
;Left::
;Right::
;Down::
;    R := 0
;    Loop{
;	 X := Y := 0
;	 MouseMove, X := GetKeyState("left","P") ? -++R : GetKeyState("right","P") ? ++R : X
;		  , Y := GetKeyState("up","P") ? -++R : GetKeyState("down","P") ? ++R : Y,,R
;	}until !X && !Y
;return
;Esc::ExitApp

